-*- mode: markdown; coding: utf-8; indent-tabs-mode: nil; word-wrap: t -*-
-*- eval: (set-language-environment Russian) -*-
--- Time-stamp: <2020-01-20 01:23:26 roukoru>

Puzzles, games with numbers, and other funny stuff

# 贤贤 #

```
printf "1- 1- 2*\n1-:1 9* 2*\n1-:1 9* 9*" | java Kenken
printf "3* 1-:1 1-:1\n3* 4+ 4+\n1-:2 1-:2 4+" | java Kenken
printf "8+ 8+ 1-:1\n1-:2 8+ 1-:1\n1-:2 3* 3*" | java Kenken
printf "2/:1 1-:1 1-:1 1-:2\n2/:1 4*:1 2/:2 1-:2\n12+  4*:1 2/:2 4*:2\n12+  12+  12+  4*:2" | java Kenken
printf "4* 1-:1 1-:1 2/:1\n4* 4* 9* 2/:1\n1-:2 2/:2 9* 9*\n1-:2 2/:2 5+ 5+" | java Kenken
printf "10*:1 10*:1 3-:1 3-:1 9+\n15* 3-:2 3-:2 6+ 9+\n15* 1- 1- 6+ 9+\n4* 4* 10*:2 2- 2-\n7+ 7+ 10*:2 2/ 2/" | java Kenken
printf "11+  2/:1 2/:1 20*  6*:1 6*:1\n11+  3-   3-   20*  3/   6*:1\n240* 240* 6*:2 6*:2 3/   6*:1\n240* 240* 6*:3 7+   30*  30*\n6*:4 6*:4 6*:3 7+   7+   9+\n8+   8+   8+   2/:2 2/:2 9+" | java Kenken
```
