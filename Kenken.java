// -*- mode: java; coding: utf-8; indent-tabs-mode: nil; word-wrap: t; -*-
// -*- eval: (set-language-environment Russian) -*-
// Time-stamp: <2020-01-20 01:33:12 roukoru>

import java.util.Objects;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Deque;
import java.util.ArrayDeque;

/**
 * 贤贤 (けんけん) game
 * en.wikipedia.org/wiki/KenKen
 * file.scirp.org/Html/1-1040464_66317.htm
 *
 * TODO:
 * 1. factorization heuristic ADD;
 * 2. factorization heuristic SUB;
 * 3. factorization heuristic MUL;
 * 4. factorization heuristic DIV;
 * 5. factorization heuristic MOD;
 */
class Kenken {
    public static void main(String[] args) {
        List <String> inData = readData();
        Suda 田 = parseData(inData);
        System.out.println(田);
        if(!solve(田))
            System.out.println("oops! fail. :(");
        System.out.println();
        System.out.println(田);
    }

    private static List <String> readData() {
        Scanner 器 = new Scanner(System.in);
        List <String> inData = new ArrayList <String> ();
        器.useDelimiter("\\n");
        for(;器.hasNext();) {
            String 串 = 器.next();
            if(串 == null) continue;
            串 = (串.split("#"))[0].trim();
            if(串.length() > 1)
                inData.add(串);
        }
        return inData;
    }

    /**
     * rows notes such as (see example from wiki:KenKen):
     *  11+     2/:1 2/:1 20*   6* 6*
     *  11+     3-   3-   20*   3/ 6*
     * 240*   240*   6*:3  6*:3 3/ 6*
     * 240*   240*   6*:1  7+  30* 30*
     *   6*:2   6*:2 6*:1  7+   7+ 9+
     *   8+     8+   8+    2/   2/ 9+
     *
     * here, 6* and 6*:1 and 6*:2 are three distinct groups of '6*'
     */
    private static Suda parseData(final List <String> data) {
        Suda 田 = new Suda(data.size());
        for(int 行 = 0; 行 < data.size(); ++行) {
            String 串 = data.get(行);
            String[] 甲 = 串.trim().split(" +");
            for(int 列 = 0; 列 < 甲.length; ++列) {
                Kumo 别 = null;
                if(田.别表.containsKey(甲[列]))
                    别 = 田.别表.get(甲[列]);
                else {
                    别 = new Kumo(甲[列]);
                    田.别表.put(甲[列], 别);
                }
                Su 丁 = new Su();
                丁.数田 = 别;
                田.数田[行][列] = 丁;
                别.add(丁);
            }
        }
        return 田;
    }

    private static boolean solve(final Suda kengrid) {
        // scrapLoneDogs(集团); fixme
        return solveRecursive(kengrid, 0, 0);
    }

    private static void scrapLoneDogs(final Deque <Kumo> kenkumo) {
        for(int 甲 = 0, 长 = kenkumo.size(); 甲 < 长; ++甲) {
            Kumo 组 = kenkumo.poll();
            if(组 == null) break;
            if(组.数表.size() == 1)
                组.数表.get(0).setFinal(组.值);
            else
                kenkumo.add(组);
        }
    }

    private static boolean solveRecursive(final Suda kengrid,
                                          final int x, final int y) {
        boolean solvedF = false;
        int nx = x + 1, ny = y;
        if(nx >= kengrid.getSize()) {
            nx = 0;
            ny = y + 1;
        }
        if(kengrid.get(x, y).isUndef()) {
            int 掩码 = 0; // ? long
            for(int 位 = 0; 位 < kengrid.getSize(); ++位) {
                if(kengrid.get(位, y).isDef())
                    掩码 |= 1 << kengrid.get(位, y).get();
                if(kengrid.get(x, 位).isDef())
                    掩码 |= 1 << kengrid.get(x, 位).get();
            }
            for(int 值 = 1; 值 <= kengrid.getSize(); ++值) {
                if((掩码 & (1 << 值)) != 0) continue;
                kengrid.set(x, y, 值);
                if(ny < kengrid.getSize()) {
                    if(solveRecursive(kengrid, nx, ny))
                        return true;
                }
                else
                    if(kengrid.checkField())
                        return true;
            }
        }
        else
            if(ny >= kengrid.getSize())
                solvedF = kengrid.checkField();
        if(!solvedF)
            kengrid.get(x, y).reset();
        return solvedF;
    }
}

class Suda { // 数田
    final int 长, // square field
        行值; // 
    final Su[][] 数田;
    final Map <String,Kumo> 别表;

    Suda(int size) {
        长 = size;
        行值 = (size + 1) * size / 2 + (size & 1) == 1 ? (size / 2 + 1) : 0; // Ave, Gauß!
        别表 = new HashMap <String,Kumo> ();
        数田 = new Su[size][];
        for(int 甲 = 0; 甲 < size; ++甲) {
            数田[甲] = new Su[size];
            for(int 乙 = 0; 乙 < 数田[甲].length; ++乙)
                数田[甲][乙] = new Su();
        }
    }

    /**
     * @return true если поле заполнено корректно
     */
    public boolean checkField() {
        // System.out.println(this);
        return testCompleteness() //
            && testLatinSquare() //
            && testGroups();
    }

    private boolean testCompleteness() {
        for(int 行 = 0; 行 < 长; ++行) // search 4 unfilled cells
            for(int 列 = 0; 列 < 长; ++列)
                if(数田[行][列].isUndef()) {
                    // System.out.printf("fail: undef %d %d\n", 列, 行);
                    return false;
                }
        return true;
    }

    private boolean testLatinSquare() {
        final int 掩码 = (1 << (长+1)) - 2;
        for(int 点 = 0; 点 < 长; ++点) { // testing 4 "latin square" condition
            int 掩码行 = 0, 掩码列 = 0;
            for(int 标 = 0; 标 < 长; ++标) {
                掩码行 |= 1 << 数田[点][标].get();
                掩码列 |= 1 << 数田[标][点].get();
            }
            if(掩码行 != 掩码 || 掩码列 != 掩码) {
                // System.out.printf("fail: m-square %d\n", 点);
                return false;
            }
        }
        return true;
    }

    private boolean testGroups() {
        for(Kumo 集合 : 别表.values())
            if(!集合.test()) {
                // System.out.printf("fail: group %s\n", 集合);
                return false;
            }
        return true;
    }

    public int getSize() {return 长;}

    public Su get(final int x, final int y) {return 数田[y][x];}

    public void set(final int x, final int y, final int val) {
        数田[y][x].set(val);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(int 行 = 0; 行 < 长; ++行) {
            boolean 身 = false;;
            for(int 列 = 0; 列 < 长; ++列) {
                if(身)
                    sb.append(' ');
                else
                    身 = true;
                sb.append(数田[行][列]);
            }
            sb.append('\n');
        }
        for(String 键 : 别表.keySet()) {
            sb.append('\n');
            sb.append(别表.get(键));
        }
        return sb.toString();
    }
}

class Su { // 数
    final static int UNDEF = 0;
    private int 值;
    Kumo 数田;
    private boolean isMutable;

    Su() {
        值 = UNDEF;
        数田 = null;
        isMutable = true;
    }

    public boolean isUndef() {return 值 < 1;}
    public boolean isDef() {return 值 > 0;}
    public int get() {return 值;}
    public boolean offer(final int n) {return 数田.offer(n);}

    public void reset() {
        if(isMutable)
            值 = UNDEF;
    }

    public void set(int n) {
        if(isMutable)
            值 = n;
    }

    public void setFinal(int n) {
        if(isMutable) {
            值 = n;
            isMutable = false;
        }
    }

    public String toString() {
        return String.format("%s(%s)",
                             值 < 1 ? "?" : String.valueOf(值),
                             数田.getFullGroup());
    }
}

class Kumo {
    final KumoOp op;
    final int 值, gid;
    List <Su> 数表;

    Kumo(String str) {
        String 串;
        int 位 = str.indexOf(":");
        if(位 < 0) {
            串 = str;
            gid = -1;
        }
        else {
            串 = str.substring(0, 位);
            gid = Integer.parseInt(str.substring(位+1));
        }
        op = KumoOp.getOp(串.charAt(串.length()-1));
        值 = Integer.parseInt(串.substring(0, 串.length()-1));
        数表 = new ArrayList <Su> ();
    }

    public void add(final Su number) {
        数表.add(number);
    }

    public String getFullGroup() {
        if(gid < 0)
            return String.format("%d%c", 值, op.getc());
        return String.format("%d%c:%d", 值, op.getc(), gid);
    }

    public boolean test() {return eval() == 值;}

    public int eval() {
        switch(op) {
        case ADD:
            return evalSum();
        case SUB:
            return evalSub();
        case MUL:
            return evalMul();
        case DIV:
            return evalDiv();
        case MOD:
            return evalMod();
        }
        return 0;
    }

    private int evalSum() {
        int 总 = 0;
        for(Su 点 : 数表)
            if(点.isUndef())
                return Su.UNDEF; // exception? ... or, NAN if fine too. >^_^<
            else
                总 += 点.get();
        return 总;
    }

    private int evalMul() {
        int 总 = 1;
        for(Su 点 : 数表)
            if(点.isUndef())
                return Su.UNDEF;
            else
                总 *= 点.get();
        return 总;
    }

    private int evalSub() {
        final int 引 = getIndexOfMaxNumber();
        if(引 < 0)
            return Su.UNDEF;
        int 总 = 数表.get(引).get();
        for(int 标 = 0; 标 < 数表.size(); ++标)
            if(数表.get(标).isUndef())
                return Su.UNDEF;
            else
                if(标 != 引)
                    总 -= 数表.get(标).get();
        return 总;
    }

    private int evalDiv() {
        final int 引 = getIndexOfMaxNumber();
        if(引 < 0)
            return Su.UNDEF;
        int 总 = 数表.get(引).get();
        for(int 标 = 0; 标 < 数表.size(); ++标)
            if(数表.get(标).isUndef())
                return Su.UNDEF;
            else
                if(标 != 引)
                    总 /= 数表.get(标).get();
        return 总;
    }

    private int evalMod() {
        /*
         * todo : reorder list, because consequent mod isn't commutative.
         * example: (11 4 3) vs (11 3 4), 2 and 0 resp.
         */
        final int 引 = getIndexOfMaxNumber();
        if(引 < 0)
            return Su.UNDEF;
        int 总 = 数表.get(引).get();
        for(int 标 = 0; 标 < 数表.size(); ++标)
            if(数表.get(标).isUndef())
                return Su.UNDEF;
            else
                if(标 != 引)
                    总 %= 数表.get(标).get();
        return 总;
    }

    private int getIndexOfMaxNumber() {
        int 数 = Integer.MIN_VALUE,
            索引 = -1;
        for(int 标 = 0; 标 < 数表.size(); ++标)
            if(数 < 数表.get(标).get()) {
                数 = 数表.get(标).get();
                索引 = 标;
            }
        return 索引;
    }

    /**
     * @return true if n is in allowed number set of this group
     */
    public boolean offer(final int n) {
        return true;
    }

    /**
     * creates set of allowed numbers for this group, (x_i|[1,size]).
     * Such as (for size 6):
     * 11+ len 2 : {5,6};
     * 20* len 2 : {4,5};
     * 6* len 4 : {1,1,2,3};
     * 2/ len 2 : {1,2} U {2,4} U {3,6};
     * 240* len 4: {2,4,5,6} U {3,4,4,5};
     */
    public void factorize(final int size) {
        ;
    }

    public int hashCode() {return Objects.hash(op, 值, gid);}

    public boolean equals(final Object obj) {
        if(obj == null || !(obj instanceof Kumo)) return false;
        Kumo kobj = (Kumo)obj;
        return kobj.op == op && kobj.gid == gid && kobj.值 == 值;
    }

    public String toString() {
        return getFullGroup();
    }
}

enum KumoOp {
    ADD, SUB, MUL, DIV, MOD, UNDEF;

    public char getc() {
        switch(this) {
        case ADD:
            return '+';
        case SUB:
            return '-';
        case MUL:
            return '*';
        case DIV:
            return '/';
        case MOD:
            return '%';
        }
        return '?';
    }

    public static KumoOp getOp(char c) {
        switch(c) {
        case '+':
            return KumoOp.ADD;
        case '-':
            return KumoOp.SUB;
        case '*':
        case 'x':
            return KumoOp.MUL;
        case '/':
            return KumoOp.DIV;
        case '%':
            return KumoOp.MOD;
        }
        throw new IllegalArgumentException();
    }
}

/**
 * iterates over set of permutations:
 * 0:[1,...,n]
 * ...
 * n!-1:[n,...,1]
 */
/*
class PermutationIterator { // implements Iterator ?
    private int[] 集合;
    private int 数, 数尾;

    PermutationIterator(int sizeof) {
        集合 = new int[sizeof];
        for(int 甲 = 0; 甲 < sizeof;)
            集合[甲++] = 甲;
        数 = 0;
        数尾 = ; // n!
    }

    public boolean hasNext() {
        return 数 < 数尾;
    }

    public int[] next() {
        ++数;
        return Arrays.copyOf(集合, 集合.length);
    }
}
*/
